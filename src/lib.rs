use std::io;
use std::io::{Read, Write};
use termion::raw::IntoRawMode;

pub enum Error{
    Exit,
    Unknown,
}

pub fn getchar() -> Result<u8, Error> {
    let mut buffer = [0];
    let stdout = io::stdout().into_raw_mode().unwrap();
    let mut stdin = io::stdin();

    stdout.lock().flush().unwrap();

    if stdin.read_exact(&mut buffer).is_ok() {
        if buffer[0] == 3 {
            Err(Error::Exit)
        } else {
            Ok(buffer[0])
        }
    } else {
        Err(Error::Unknown)
    }
}
